import { MOVIES_ERROR, MOVIES_LOADING, MOVIES_LIST_RESULTS, MOVIE_DETAILS_RESULTS, SEARCH_RESULT, SEARCH_ERROR, SEARCH_QUERY } from "../constants/constants";
import { combineReducers } from 'redux';

export function movieHasErrored(state = false, action) {
    switch (action.type) {
        case MOVIES_ERROR:
            return action.hasErrored;
        default:
            return state;
    }
}

export function movieIsLoading(state = false, action) {
    switch (action.type) {
        case MOVIES_LOADING:
            return action.isLoading;
        default:
            return state;
    }
}

export function moviesListItems(state = {}, action) {
    switch (action.type) {
        case MOVIES_LIST_RESULTS:
            return action.movies;
        default:
            return state;
    }
}

export function movieDetailsItem(state = {}, action) {
    switch (action.type) {
        case MOVIE_DETAILS_RESULTS:
            return action.movie;
        default:
            return state;
    }
}

// SEARCH
export function movieSearchItems(state = {}, action) {
    switch (action.type) {
        case SEARCH_RESULT:
            return action.movies;
        default:
            return state;
    }
}

export function movieSearchError(state = false, action) {
    switch (action.type) {
        case SEARCH_ERROR:
            return action.error;
        default:
            return state;
    }
}

export function movieSearchQuery(state = "", action) {
    switch (action.type) {
        case SEARCH_QUERY:
            return action.searchQuery;
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    movieHasErrored, 
    movieIsLoading, 
    moviesListItems,
    movieDetailsItem,
    movieSearchItems,
    movieSearchError,
    movieSearchQuery
});

export default rootReducer;