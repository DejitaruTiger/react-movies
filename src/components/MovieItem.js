import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { IMG_URL } from '../constants/constants';
import { Link } from 'react-router-dom';

class Movie extends Component {

    render() {
        return (
            <div className="row">
                <div className="col s12 l12">
                    <Link to={`/details/${this.props.movie.id}`}>
                        <div className="card" >
                            <div className="card-image">
                                <img src={IMG_URL + this.props.movie.poster_path} alt={this.props.movie.overview}/>
                            </div>
                            {/* <div className="card-content"> */}
                                <p className="card-title" style={{fontSize: 16, paddingBottom: '10px'}}>{this.props.movie.title}</p>
                            {/* </div> */}
                        </div>
                    </Link>
                </div>
            </div>
        );
    }
}

Movie.propTypes = {
    movie: PropTypes.object.isRequired,
}

export default Movie;