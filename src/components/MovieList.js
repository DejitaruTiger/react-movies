import React, { Component } from 'react';
import PropTypes from 'prop-types'
import MovieItem from './MovieItem';
import './css/MovieList.css'

class MovieList extends Component {
    render() {
        let movies = this.props.items;
        if (!movies) {
            return "";
        }

        return (
            <ul className="movie-list">
                {movies.map((item) => (
                    <li className="movie-item" key={item.id}>
                        <MovieItem movie={item} />
                    </li>
                ))}
            </ul>
        );
    }
}

MovieList.propTypes = {
    items: PropTypes.array.isRequired,
}

export default MovieList;