import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class Pager extends Component {

    render() {
        let page = this.props.page ? Number(this.props.page) : 1;
        let url = this.props.url;
        let total = this.props.total ? Number(this.props.total) : 1;

        let next = Number(page) + 1;
        let previous = Number(page) - 1;
        return (
            

            <div style={divStyle}>
                { page > 1 ? 
                    <Link to={`${url}${previous}`}><button className="waves-effect waves-light btn" style={pagerStyle}>Previous</button></Link>  : 
                    "" 
                }

                { total > 1 ? 
                    <Link to={`${url}${next}`}><button className="waves-effect waves-light btn" style={pagerStyle}>Next</button></Link> : 
                    ""
                }
            </div>
        );
    }
}

Pager.propTypes = {
    url: PropTypes.string.isRequired
};

const pagerStyle = {
    marginTop: '64px',
    marginBottom: '36px',
    marginLeft: '8px',
    marginRight: '8px'
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}



export default Pager;