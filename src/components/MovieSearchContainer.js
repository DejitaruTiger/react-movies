import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';
import { searchMovies } from '../actions/movies';
import MovieSearch from './MovieSearch';


class MovieSearchContainer extends Component {

    componentDidMount() {
        let query = this.props.match.params.query;
        let page = this.props.match.params.page;

        this.props.search(query, page);
    }

    componentWillReceiveProps(newProps) {
        let query = this.props.match.params.query;
        let page = this.props.match.params.page;

        if (query !== newProps.match.params.query || page !== newProps.match.params.page) {
            this.props.search(newProps.match.params.query, newProps.match.params.page);
        }
    }

    render() {
        document.body.scrollTop = document.documentElement.scrollTop = 0; // scroll to top
        let query = this.props.match.params.query;
        if (!query) {
            return <h3>Error - Specify the query</h3>
        }

        if(this.props.error) {
            return <h6>Error...</h6>;
        }

        let searchQuery = this.props.searchQuery;
        if (searchQuery.length > 0) {
            return <MovieSearch searchItems={this.props.items}
                                searchQuery={this.props.searchQuery} 
                                page={Number(this.props.match.params.page)} />
        }

        return <Redirect to='/'/>
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.movieSearchItems,
        error: state.movieSearchError,
        searchQuery: state.movieSearchQuery
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        search: (query, page) => dispatch(searchMovies(query, page))
    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MovieSearchContainer));