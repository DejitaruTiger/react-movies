import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMovieDetails } from '../actions/movies';
import MovieDetails from './MovieDetails';
import Preloader from './Preloader';
import { withRouter } from 'react-router-dom';

class MovieDetailsContainer extends Component {

    componentDidMount() {
        var id = this.props.match.params.id;
        this.props.getMovie(id);
    }

    render() {
        let id = this.props.match.params.id;
        if (!id) {
            return <h3>Error - Specify ID</h3>
        } else if (!Number.isInteger(Number(id))) {
            return <h3>Error - Specify integer ID</h3>
        }

        if(this.props.isLoading) {
            return <Preloader />;
        }

        if(this.props.hasErrored) {
            return <h6>Error..."</h6>;
        }

        if(Object.keys(this.props.item).length !== 0) {
            return <MovieDetails movie={ this.props.item } />;
        } else {
            return "";
        }
    }
}

const mapStateToProps = (state) => {
    return {
        item: state.movieDetailsItem,
        hasErrored: state.movieHasErrored,
        isLoading: state.movieIsLoading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMovie: (url) => dispatch(getMovieDetails(url))
    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MovieDetailsContainer));