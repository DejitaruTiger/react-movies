import React, { Component } from 'react';

class Preloader extends Component {

  render() {
    return <div style={divStyle} className="preloader-wrapper big active">
      <div className="spinner-layer spinner-blue-only">
        <div className="circle-clipper left">
          <div className="circle"></div>
        </div><div className="gap-patch">
          <div className="circle"></div>
        </div><div className="circle-clipper right">
          <div className="circle"></div>
        </div>
      </div>
    </div>
  }
}

const divStyle = {
  margin: '10% 50%'
};

export default Preloader;