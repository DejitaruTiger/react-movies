import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { IMG_URL } from '../constants/constants';
import './css/MovieDetails.css';


class MovieDetails extends Component {

    render() {
        let movie = this.props.movie;
        console.log(movie);

        if (!movie) {
            return <h5>Error! </h5>
        }

        document.title = movie.title;
        return (
            <div className="row" style={{padding: '10px'}}>
                <div className="col l3 s12">
                    <div className="card" style={{padding: '10px'}}>
                        <img style={{width: '100%', height: '100%'}} src={IMG_URL + movie.poster_path} alt={movie.overview} /> 
                    </div>
                </div>
                <div className="col l9 s12"> 
                    <div className="card" style={{padding: '10px'}}>       
                        <h4>{movie.title}</h4>
                        <h6>{movie.overview}</h6>
                        <p>Дата релізу: {movie.release_date}</p>
                        <p>{movie.vote_average}</p>

                        {movie.genres ? movie.genres.map((item) => (
                            <div className="chip" key={item.id}>{item.name}</div>
                        )) : ""}
                    </div>
                </div>
            </div>
        );
    }
}

MovieDetails.propTypes = {
    movie: PropTypes.object.isRequired,
}

export default MovieDetails;