import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchMovies, searchQuery } from '../actions/movies';
import { debounce } from '../utils/utils';

class Search extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);

        this.date = Date.now();
        this.oldDate = 0;
    }

    onChange(e) {
        let value = e.target.value;        
        debounce(() => {
            this.props.query(value); // Send current search query
            if (value.length === 0) return;
            this.props.search(value, 1);
        }, 300)();
    }

    render() {
        return <div style={divStyle}>
                <input style={inputStyle} type="text" placeholder="Поиск" defaultValue={this.props.searchQuery} onChange={this.onChange} />
            </div>
    }
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
};

const inputStyle = {
    marginTop: '20px',
    marginLeft: '30px',
    marginRight: '30px',
    width: '65%'
};

const mapStateToProps = (state) => {
    return {
        searchQuery: state.movieSearchQuery
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        search: (query, page) => dispatch(searchMovies(query, page)),
        query: (query) => dispatch(searchQuery(query))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);