import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MovieList from './MovieList';
import Pager from './Pager';
import Search from './Search';

class MovieSearch extends Component {

    render() {
        let searchItems = this.props.searchItems;

        if (searchItems && Object.keys(searchItems).length > 0) {
            if (searchItems.results.length > 0) {
                return (
                    <div>
                        <div>
                            <Search />
                        </div>
                        <MovieList items={searchItems.results} />
                        <div>
                            <Pager page={this.props.page} 
                                   total={searchItems.total_pages} 
                                   url={`/search/${this.props.searchQuery}/`} />
                        </div>
                    </div>
                );
            }
        }

        return <b>No data</b>
    }
}

MovieSearch.propTypes = {
    page: PropTypes.number.isRequired,
    searchItems: PropTypes.object.isRequired,
    searchQuery: PropTypes.string.isRequired
};

export default MovieSearch;