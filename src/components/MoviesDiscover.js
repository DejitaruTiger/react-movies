import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { dicoverMovies } from '../actions/movies';
import MovieList from './MovieList';
import Preloader from './Preloader';
import { withRouter } from 'react-router-dom';
import Pager from './Pager';
import { isObjectNotEmpty } from '../utils/utils';

class MoviesDiscover extends Component {
    componentDidMount() {
        this.props.dicoverMovies(this.props.page);
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.page !== nextProps.page) {
            this.props.dicoverMovies(nextProps.page);
        }
    }

    render() {
        document.body.scrollTop = document.documentElement.scrollTop = 0; // sroll to top

        if (this.props.hasErrored) {
            return <p>Sorry! There was an error loading the items</p>;
        }

        if(this.props.searchError) {
            return <p>Search Error</p>
        }

        if (this.props.isLoading) {
            return <Preloader />;
        }

        // Search Items
        let searchItems = this.props.searchItems;
        if (this.props.searchQuery.length > 0) {
            if (searchItems && isObjectNotEmpty(searchItems)) {
                if (searchItems.results.length > 0) {
                    return (
                        <div>
                            <MovieList items= {searchItems.results} />
                            <div>
                                <Pager page={1} total={searchItems.total_pages} url={`/search/${this.props.searchQuery}/`} />
                            </div>
                        </div>
                    );
                }
                return <p>No results</p>
            }
        }

        // Discover
        if (this.props.items && isObjectNotEmpty(this.props.items)) {
            console.log(this.props.items);
            return (
                <div>
                    <MovieList items= {this.props.items.results} />
                    <div>
                        <Pager page={this.props.page} total={this.props.items.total_pages}  url="/" />
                    </div>
                </div>
            );
        } else {
            return <b>No data</b>
        }
    }
}

MoviesDiscover.propTypes = {
    hasErrored: PropTypes.bool.isRequired,
    items: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
}
  
const mapStateToProps = (state) => {
    return {
        items: state.moviesListItems,
        hasErrored: state.movieHasErrored,
        isLoading: state.movieIsLoading,
        searchItems: state.movieSearchItems,
        searchError: state.movieSearchError,
        searchQuery: state.movieSearchQuery
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dicoverMovies: (page) => dispatch(dicoverMovies(page))
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MoviesDiscover));