import React, { Component } from 'react';
import MoviesDiscover from './MoviesDiscover';
import Search from './Search';

class Main extends Component {
    render() {
        let page = 1;
        if (this.props.match) {
            page = this.props.match.params.page;
        }

        return (
            <div>
                <div>
                    <Search />
                </div>
                <MoviesDiscover page={page} />
            </div>);
    }
}

export default Main;