// URLs
export const API_KEY = 'e079efd1bb898b2795ce64b721d6a81f';
export const SERVER_URL = 'https://api.themoviedb.org/';
export const IMG_URL = 'https://image.tmdb.org/t/p/w500/';

export const URL_MOVIE_DETAILS = (id) => '3/movie/' + id;
export const URL_MOVIE_DISCOVER = '3/discover/movie';
export const URL_MOVIE_SEARCH = '3/search/movie';

// Reducers

// Actions
export const MOVIES_ERROR = 'MOVIES_DISCOVER_ERROR';
export const MOVIES_LOADING = 'MOVIES_DISCOVER_LOADING';
export const MOVIES_LIST_RESULTS = 'MOVIES_LIST_RESULTS';
export const MOVIE_DETAILS_RESULTS = 'MOVIE_DETAILS_RESULTS';
export const SEARCH_ERROR = 'SEARCH_ERROR';
export const SEARCH_RESULT = 'SEARCH_RESULT';
export const SEARCH_QUERY = 'SEARCH_QUERY';