import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Main from '../components/Main'
import MovieDetailsContainer from '../components/MovieDetailsContainer';
import MovieSearchContainer from '../components/MovieSearchContainer';


const Routes = () => {
    return <Switch>
        <Route path="/details/:id" component={MovieDetailsContainer} />
        <Route path="/search/:query/:page" component={MovieSearchContainer} />
        <Route path="/:page" component={Main} />
        <Route exact path="/" component={Main} />
    </Switch>;
}

export default Routes;