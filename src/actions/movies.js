import { MOVIES_LOADING, URL_MOVIE_DISCOVER, MOVIES_ERROR, MOVIES_LIST_RESULTS, URL_MOVIE_DETAILS, MOVIE_DETAILS_RESULTS, URL_MOVIE_SEARCH, SEARCH_ERROR, SEARCH_RESULT, SEARCH_QUERY } from '../constants/constants';
import API from '../api/api';

export function moviesHasErrored(bool) {
    return {
        type: MOVIES_ERROR,
        hasErrored: bool
    };
}

export function moviesIsLoading(bool) {
    return {
        type: MOVIES_LOADING,
        isLoading: bool
    };
}

export function moviesListDataSuccess(items) {
    return {
        type: MOVIES_LIST_RESULTS,
        isLoading: false,
        movies: items
    };
}

export function movieDetailsDataSuccess(item) {
    return {
        type: MOVIE_DETAILS_RESULTS,
        isLoading: false,
        movie: item
    };
}

export function searchError(bool) {
    return {
        type: SEARCH_ERROR,
        error: bool
    };
}

export function searchResult(items) {
    return {
        type: SEARCH_RESULT,
        movies: items,
    };
}


export function dicoverMovies(page) {
    return (dispatch) => {
        dispatch(moviesIsLoading(true));

        API.get(URL_MOVIE_DISCOVER, {
            params: {
                'page': page
            }
        })
          .then(response => {
            dispatch(moviesIsLoading(false));
            dispatch(moviesListDataSuccess(response.data));
          })
          .catch(e => {
            console.log(e);
            dispatch(moviesHasErrored(true))
          });
    };
}

export function getMovieDetails(id) {
    return (dispatch) => {
        dispatch(moviesIsLoading(true));

        API.get(URL_MOVIE_DETAILS(id))
          .then(response => {
            dispatch(moviesIsLoading(false));
            dispatch(movieDetailsDataSuccess(response.data));
          })
          .catch(e => {
            console.log(e);
            dispatch(moviesHasErrored(true))
          });
    };
}

export function searchMovies(query, page) {
    return (dispatch) => {
        API.get(URL_MOVIE_SEARCH, {
          params: {
              'query': query,
              'page': page
          }  
        })
          .then(response => {
            dispatch(searchResult(response.data, query));
          })
          .catch(e => {
            console.log(e);
            dispatch(searchError(true));
          })
    };
}

export function searchQuery(query) {
    return {
        type: SEARCH_QUERY,
        searchQuery: query
    };
}