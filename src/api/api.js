import axios from 'axios';
import { SERVER_URL, API_KEY } from '../constants/constants';

var userLang = navigator.language || navigator.userLanguage; 

export default axios.create({
  baseURL: SERVER_URL,
  params: {
    language: 'ru',
    api_key: API_KEY
  }
});